import React, { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Button,
  TextInput,
  Image,
  Picker,
  ScrollView,
} from "react-native";

// import { Container, Header, Content, Textarea, Form } from "native-base";

export default function Register(props) {
  const handle = () => {
    props.navigation.navigate("Login");
  };
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [fullname, setFullname] = React.useState("");
  const [gender, setGender] = React.useState("");
  const [phone, setPhone] = React.useState("");
  const [address, setAddress] = React.useState("");
  return (
    <View style={styles.container}>
      <Image
        source={require(`../../assets/Bigiconliterature.png`)}
        style={{
          marginBottom: 5,
          width: 300,
          height: 75,
          resizeMode: "stretch",
        }}
      />

      <TextInput
        style={styles.input}
        placeholderTextColor={"white"}
        placeholder={"Email"}
        keyboardType={"email-address"}
        value={email}
        onChangeText={setEmail}
      />

      <TextInput
        style={styles.input}
        placeholderTextColor={"white"}
        placeholder={"Password"}
        secureTextEntry
        value={password}
        onChangeText={setPassword}
      />

      <TextInput
        style={styles.input}
        placeholderTextColor={"white"}
        placeholder={"Fullname"}
        value={fullname}
        onChangeText={setFullname}
      />

      <Picker
        mode="dropdown"
        selectedValue={gender}
        style={styles.input}
        onValueChange={setGender}
      >
        <Picker.Item label="Gender" value="" />
        <Picker.Item label="Male" value="Male" />
        <Picker.Item label="Female" value="Female" />
      </Picker>

      <TextInput
        style={styles.input}
        placeholderTextColor={"white"}
        placeholder={"Phone Number"}
        value={phone}
        onChangeText={setPhone}
      />

      <TextInput
        style={styles.input}
        placeholderTextColor={"white"}
        placeholder={"Address"}
        value={address}
        onChangeText={setAddress}
      />

      <TouchableOpacity
        style={styles.containerBtnLogin}
        onPress={() => props.navigation.navigate("Login")}
      >
        <Text style={styles.textBtnLogin}>Register</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.containerRegister}
        onPress={() => {
          props.navigation.navigate("Login");
        }}
      >
        <Text style={[styles.textRegister]}>
          Already Have Account? Login Here
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    backgroundColor: "rgba(210, 210, 210, 0.25)",
    width: "85%",
    padding: 10,
    borderRadius: 8,
    color: "black",
    marginVertical: 8,
    fontSize: 16,
    color: "white",
  },
  containerBtnLogin: {
    width: "85%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 9,
    padding: 15,
    borderRadius: 8,
    backgroundColor: "#AF2E1C",
  },
  textBtnLogin: {
    color: "white",
    fontWeight: "500",
    fontSize: 18,
  },
  containerRegister: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },
  textRegister: {
    fontWeight: "500",
    fontSize: 14,
    color: "#fff",
  },
});
