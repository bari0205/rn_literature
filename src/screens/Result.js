import React, { useState, useEffect } from "react";
import { API } from "../config/Api";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Button,
  TextInput,
  Image,
  FlatList,
} from "react-native";
import { ListItem, Avatar } from "react-native-elements";
import axios from "axios";

export default function Result(props) {
  const [search, setSearch] = React.useState("");
  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    console.log("Component Did Mount");
    getData();
  }, []);

  const getData = () => {
    setIsLoading(true);
    axios
      .get(`http://localhost:5000/api/v1/books`)

      .then((res) => {
        console.log("response success");
        setList(res.data.data.all);
        setIsLoading(false);
        console.log(res);
      })
      .catch((err) => {
        console.log("response error");
        console.log(err);
        setIsLoading(false);
      });
  };

  const renderItem = ({ item }) => {
    return (
      <ListItem key={item.id} bottomDivider>
        <Avatar source={{ uri: require(`../../assets/${item.image}`) }} />
        <ListItem.Content>
          <ListItem.Title>{item.title}</ListItem.Title>
          <ListItem.Subtitle>{item.author}</ListItem.Subtitle>
        </ListItem.Content>
      </ListItem>
    );
  };
  return (
    <View style={styles.container}>
      <Image
        source={require(`../../assets/Bigiconliterature.png`)}
        style={{
          marginBottom: 30,
          marginLeft: -200,
          width: 100,
          height: 30,
          resizeMode: "stretch",
        }}
      />
      <TextInput
        style={styles.input}
        placeholderTextColor={"white"}
        placeholder={"Search Literature"}
        value={search}
        onChangeText={setSearch}
        require
      />
      <TouchableOpacity
        style={styles.containerBtnLogin}
        onPress={() => props.navigation.navigate("Login")}
      >
        <Text style={styles.textBtnLogin}>Search</Text>
      </TouchableOpacity>

      <View style={{ height: "385px", marginTop: "10%" }}>
        <FlatList
          style={{ backgroundColor: "black" }}
          data={list}
          renderItem={renderItem}
          refreshing={isLoading}
          onRefresh={getData}
          keyExtractor={(item) => item.id}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    backgroundColor: "rgba(210, 210, 210, 0.25)",
    width: "85%",
    padding: 10,
    borderRadius: 8,
    color: "black",
    marginVertical: 8,
    fontSize: 16,
    color: "white",
  },
  containerBtnLogin: {
    width: "85%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 9,
    padding: 15,
    borderRadius: 8,
    backgroundColor: "#AF2E1C",
  },
  textBtnLogin: {
    color: "white",
    fontWeight: "500",
    fontSize: 18,
  },
});
