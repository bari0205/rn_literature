import React, { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Button,
  TextInput,
  Image,
} from "react-native";

import { useQuery, useMutation } from "react-query";

export default function Home(props) {
  const [search, setSearch] = React.useState("");
  return (
    <View style={styles.container}>
      <Image
        source={require(`../../assets/Bigiconliterature.png`)}
        style={{
          marginBottom: 5,
          width: 300,
          height: 75,
          resizeMode: "stretch",
        }}
      />
      <TextInput
        style={styles.input}
        placeholderTextColor={"white"}
        placeholder={"Search Literature"}
        value={search}
        onChangeText={setSearch}
        require
      />
      <TouchableOpacity
        style={styles.containerBtnLogin}
        onPress={() => props.navigation.navigate("Result")}
      >
        <Text style={styles.textBtnLogin}>Search</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    backgroundColor: "rgba(210, 210, 210, 0.25)",
    width: "85%",
    padding: 10,
    borderRadius: 8,
    color: "black",
    marginVertical: 8,
    fontSize: 16,
    color: "white",
  },
  containerBtnLogin: {
    width: "85%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 9,
    padding: 15,
    borderRadius: 8,
    backgroundColor: "#AF2E1C",
  },
  textBtnLogin: {
    color: "white",
    fontWeight: "500",
    fontSize: 18,
  },
});
