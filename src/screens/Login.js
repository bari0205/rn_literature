import React, { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Button,
  TextInput,
  Image,
} from "react-native";

export default function Login(props) {
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  return (
    <View style={styles.container}>
      <Image
        source={require(`../../assets/Bigiconliterature.png`)}
        style={{
          marginBottom: 5,
          width: 300,
          height: 75,
          resizeMode: "stretch",
        }}
      />
      <TextInput
        style={styles.input}
        placeholderTextColor={"white"}
        placeholder={"Email"}
        keyboardType={"email-address"}
        value={email}
        onChangeText={setEmail}
      />
      <TextInput
        style={styles.input}
        placeholderTextColor={"white"}
        placeholder={"Password"}
        secureTextEntry
        value={password}
        onChangeText={setPassword}
      />
      <TouchableOpacity
        style={styles.containerBtnLogin}
        onPress={() => props.navigation.navigate("Home")}
      >
        <Text style={styles.textBtnLogin}>Login</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.containerRegister}
        onPress={() => {
          props.navigation.navigate("Register");
        }}
      >
        <Text style={[styles.textRegister]}>
          Don't Have Account? Register Here
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    backgroundColor: "rgba(210, 210, 210, 0.25)",
    width: "85%",
    padding: 10,
    borderRadius: 8,
    color: "black",
    marginVertical: 8,
    fontSize: 16,
    color: "white",
  },
  containerBtnLogin: {
    width: "85%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 9,
    padding: 15,
    borderRadius: 8,
    backgroundColor: "#AF2E1C",
  },
  textBtnLogin: {
    color: "white",
    fontWeight: "500",
    fontSize: 18,
  },
  containerRegister: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },
  textRegister: {
    fontWeight: "500",
    fontSize: 14,
    color: "#fff",
  },
});
