import axios from "axios";

//set base URL || Agar tidak mengulangi penulisan url ditiap page
export const API = axios.create({
  baseURL: "http://localhost:5000/api/v1",
});
