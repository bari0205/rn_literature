import { StatusBar } from "expo-status-bar";
import "react-native-gesture-handler";
import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Login from "./src/screens/Login";
import Register from "./src/screens/Register";
import Home from "./src/screens/Home";
import Result from "./src/screens/Result";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <StatusBar />
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={Login}
          options={{
            title: "Login User or Admin",
          }}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{
            title: "Register User",
          }}
        />

        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            title: "Seacrh Literature",
          }}
        />

        <Stack.Screen
          name="Result"
          component={Result}
          options={{
            title: "Result",
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
